using abp.DTOS.Characters;
using abp.Models;
using abp.Services;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;

namespace abp.Controllers;

[ApiController]
[Route("api/[controller]")]
public class CharacterController:ControllerBase
{
    private readonly CharacterService _characterService;

    public CharacterController(CharacterService characterService)
    {
        _characterService = characterService;
    }
    
    [HttpGet("getall")]
    
    public ActionResult<ResultCharacter> GetAll()
    {
        return Ok(_characterService.GetAllCharacters());
    }
    
    
    [HttpPost("add")]
    
    public ActionResult<ResultCharacter> PostAll(AddCharacterDto characters)
    {
        return Ok(_characterService.AddCharacters(characters));
    }
    
    
    [HttpPut("put")]
    
    public ActionResult<ResultCharacter> PutAll(UpdateCharactersDto characters)
    {
        return Ok(_characterService.UpdateCharacters(characters));
    }
    [HttpDelete("delete/{id}")]
    
    public ActionResult<CharacterDTO> DeleteObject(int id)
    {
        return Ok(_characterService.DeleteCharacter(id));
    }
    [HttpGet("get/{id}")]
    
    public ActionResult<CharacterDTO> GetbyId(int id)
    {
        return Ok(_characterService.GetbyId(id));
    }
}