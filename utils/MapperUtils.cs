using abp.Models;

namespace abp.utils
{
    using AutoMapper;
    using abp.DTOS.Characters;

    public  class MapperUtils
    {
        private  MapperConfiguration _configMapper = new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<Characaters, CharacterDTO>();
            cfg.CreateMap<AddCharacterDto, Characaters>();
            cfg.CreateMap<UpdateCharactersDto, Characaters>();
            
            
            
        });

        public IMapper GetMapper() => _configMapper.CreateMapper();
    }
}