using abp.DTOS.Characters;
using abp.Models;
using Microsoft.EntityFrameworkCore;

namespace abp.Interfaces;

public interface ICharacter
{
    Task<ResultCharacter> GetAllCharacters();
    Task<ResultCharacter> AddCharacters(AddCharacterDto newCharacter);
    Task<ResultCharacter> UpdateCharacters(UpdateCharactersDto newCharacter);

    Task<ResultCharacter?> DeleteCharacter(int id);
    Task<CharacterDTO> GetbyId(int id);
}