using abp.ENUM;

namespace abp.DTOS.Characters;

public class AddCharacterDto
{
    
    public string Name { get; set; } 
    public int Hitpoints { get; set; } 
    public int Strength { get; set; } 
    public int Defense { get; set; } 
    public int Inteligence { get; set; }
    public CharacterClass Class { get; set; }

    private bool HasName => !string.IsNullOrEmpty(Name);
}