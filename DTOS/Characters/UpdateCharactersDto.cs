using abp.ENUM;

namespace abp.DTOS.Characters;

public class UpdateCharactersDto
{
    public int Id { get; set; }
    public string Name { get; set; } = "Frodo";
    public int Hitpoints { get; set; } = 100;
    public int Strength { get; set; } = 10;
    public int Defense { get; set; } = 10;
    public int Inteligence { get; set; } = 10;
    public CharacterClass Class { get; set; } = CharacterClass.Mage;
}