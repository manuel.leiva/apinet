using abp.DTOS.Characters;
using abp.Helpers;
using abp.Interfaces;
using abp.Models;
using abp.utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace abp.Services;

public class CharacterService : ICharacter
{
    private readonly MapperUtils _mapper;
    private readonly DataContext _context;


    public CharacterService(MapperUtils mapper, DataContext context)
    {
        _mapper = mapper;
        _context = context;
    }

    public ResultCharacter list = new ResultCharacter
    {
        CharacatersList = new List<CharacterDTO>(), // Initialize with an empty list
    };

    public async Task<ResultCharacter> GetAllCharacters()
    {
        var db = _context.Character.ToList().OrderBy(i=>i.Id);
        var data = db.Select(c => _mapper.GetMapper().Map<CharacterDTO>(c)).ToList();
        var obj = new ResultCharacter { CharacatersList = data };
        return await Task.FromResult(obj);
    }

    public Task<ResultCharacter> AddCharacters(AddCharacterDto newCharacter)
    {
        Characaters characaters = _mapper.GetMapper().Map<Characaters>(newCharacter);
        _context.Character.Add(characaters);
        _context.SaveChanges();
        var data = _context.Character.Select(c => _mapper.GetMapper().Map<CharacterDTO>(c)).ToList();
        var obj = new ResultCharacter { CharacatersList = data };

        return  Task.FromResult(obj);
    }




    public  Task<ResultCharacter> UpdateCharacters(UpdateCharactersDto newCharacter)
    {
        var character =  _context.Character.Find(newCharacter.Id);
        List<CharacterDTO> list = new List<CharacterDTO>();
        if (character != null)
        {
            _mapper.GetMapper().Map(newCharacter, character); // Update the existing character entity
             _context.SaveChanges(); // Await the SaveChangesAsync operation
             CharacterDTO updatedDto = _mapper.GetMapper().Map<CharacterDTO>(character); // Map the updated character entity to DTO
             list.Add(updatedDto);
             var obj = new ResultCharacter { CharacatersList = list };
             return   Task.FromResult(obj);
        }
        return null;
    }

    public Task<ResultCharacter?> DeleteCharacter(int id)
    {
        var dbquery = _context.Character.Find(id);
        if (dbquery != null)
        {
            _context.Character.Remove(dbquery);
             _context.SaveChanges();
            var data = _context.Character.Select(c => _mapper.GetMapper().Map<CharacterDTO>(c)).ToList();
            var obj = new ResultCharacter { CharacatersList = data };
            return Task.FromResult(obj)!;

        }

        return null;
    }

    public Task<CharacterDTO> GetbyId(int id)
    {
        var dbquery = _context.Character.Find(id);
        CharacterDTO updatedDto = _mapper.GetMapper().Map<CharacterDTO>(dbquery);
        return Task.FromResult(updatedDto);
    }
}

   
        
